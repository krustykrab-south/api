from client.views import ClientViewSet
from django.urls import include, path
from menu.views import MenuViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'menu', MenuViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
]
