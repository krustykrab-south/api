from django.db import models


class Menu(models.Model):
    TYPE_CHOICES = [
        ('Acompanhamento', 'Acompanhamento'),
        ('Bebida', 'Bebida'),
        ('Combo', 'Combo'),
        ('Hamburguer', 'Hamburguer'),
        ('Promoção', 'Promoção'),
        ('Sobremesa', 'Sobremesa'),
    ]

    title = models.CharField(max_length=80)
    description = models.CharField(max_length=160)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    type = models.CharField(max_length=14, choices=TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.title} - $ {self.price}'

    class Meta:
        db_table = 'menu'
