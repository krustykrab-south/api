from django.test import TestCase, tag

from .models import Menu


def __create_datamodel__() -> None:
    Menu.objects.create(
        title='Hamburguer de Sirí King',
        description='Combo Hamburguer de Sirí com batata frita e refrigerante',
        price=19.99,
        type='Combo'
    )
    Menu.objects.create(
        title='Hamburguer de Sirí Jr',
        description='Combo Hamburguer de Sirí pequeno com batata frita e refrigerante',
        price=14.99,
        type='Combo'
    )


@tag('database')
class MenuTableTestCase(TestCase):
    def setUp(self) -> None:
        __create_datamodel__()

    def tearDown(self) -> None:
        Menu.objects.all().delete()

    def test_select_data_from_menu_table(self) -> None:
        '''When select a data from Menu table then must return two datas'''
        menu = Menu.objects.filter(title__contains='Hamburguer')
        self.assertEqual(menu.count(), 2)

    def test_update_data_from_menu_table(self) -> None:
        '''When select a obejct from Menu table then must be possible chance the price'''
        menu = Menu.objects.filter(title='Hamburguer de Sirí Jr').first()
        menu.price = 11.99
        menu.save()
        self.assertEqual(menu.price, 11.99)


@tag('api')
class MenuAPITestCase(TestCase):
    def setUp(self) -> None:
        __create_datamodel__()

    def tearDown(self) -> None:
        Menu.objects.all().delete()

    def test_menu_endpoint_method_get_return_menu(self) -> None:
        """When make request with method GET to endpoint /api/menu, then return all foods on menu"""
        response_expected = [
            {
                'title': 'Hamburguer de Sirí Jr',
                'description': 'Combo Hamburguer de Sirí pequeno com batata frita e refrigerante',
                'price': '14.99',
                'type': 'Combo'
            },
            {
                'title': 'Hamburguer de Sirí King',
                'description': 'Combo Hamburguer de Sirí com batata frita e refrigerante',
                'price': '19.99',
                'type': 'Combo'
            }
        ]

        response = self.client.get('/api/menu/')

        self.assertJSONEqual(response.content.decode(), response_expected)

    def test_menu_endpoint_method_post_to_create_menu(self) -> None:
        """When POST request to endpoint /api/menu then response must be 201 and GET request must return 3 menus"""
        response_expected = [
            {
                'title': 'Hamburguer de Sirí',
                'description': 'Hamburguer de Sirí',
                'price': '9.99',
                'type': 'Hamburguer'
            },
            {
                'title': 'Hamburguer de Sirí Jr',
                'description': 'Combo Hamburguer de Sirí pequeno com batata frita e refrigerante',
                'price': '14.99',
                'type': 'Combo'
            },
            {
                'title': 'Hamburguer de Sirí King',
                'description': 'Combo Hamburguer de Sirí com batata frita e refrigerante',
                'price': '19.99',
                'type': 'Combo'
            }
        ]

        self.client.post(
            '/api/menu/',
            {
                'title': 'Hamburguer de Sirí',
                'description': 'Hamburguer de Sirí',
                'price': '9.99',
                'type': 'Hamburguer'
            }
        )

        response = self.client.get('/api/menu/')

        self.assertJSONEqual(response.content.decode(), response_expected)
