from django.test import TestCase, tag

from .models import Client


@tag('database')
class ClientTableTestCase(TestCase):
    def setUp(self):
        Client.objects.create(name='Rodrigo')

    def tearDown(self):
        Client.objects.all().delete()

    def test_select_client_table(self):
        """When select a data from client table then must return one data"""
        client = Client.objects.filter(name='Rodrigo')
        self.assertEqual(client.count(), 1)

    def test_update_client_table(self):
        """When select some client from table, then must be possible change the name to Ana"""
        client = Client.objects.get(name='Rodrigo')
        client.name = 'Ana'
        client.save()
        self.assertEqual(client.name, 'Ana')


@tag('api')
class ClientAPITestCase(TestCase):
    def setUp(self):
        Client.objects.create(name='Ana')

    def test_client_endpoint_method_get_return_clients(self):
        """When make request with method GET to endpoint /api/client, then return all clients"""
        response_expected = [{'name': 'Ana'}]

        response = self.client.get('/api/client/')

        self.assertJSONEqual(response.content.decode(), response_expected)

    def test_client_endpoint_method_post_to_create_client(self):
        """When POST request to endpoint /api/client then GET request must return 2 clients"""
        response_expected = [{'name': 'Rodrigo'}, {'name': 'Ana'}]

        self.client.post('/api/client/', {'name': 'Rodrigo'})
        response = self.client.get('/api/client/')

        self.assertJSONEqual(response.content.decode(), response_expected)
